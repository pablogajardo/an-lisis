import java.util.ArrayList;
import java.util.List;

public class Grafo {
    private List<Nodo> Nodos;
   
    public Grafo(){
        Nodos = new ArrayList<Nodo>();
    }
    // procedimientos
    public void agregarNodo(String nom){
        if(this.BuscarNodo(nom)==null){
        Nodo nuevo = new Nodo(nom);
        Nodos.add(nuevo);
        }else{
            System.out.println("Este Nodo ya existe");
        }
    }
    
    public void eliminarNodo(String n)
    {
        Integer ContNodo  = 0;
       if(this.Nodos.isEmpty()){
           
           System.out.println("Grafo Vacio ");
       }else{
           
           for (Nodo TempNodo : this.Nodos){
               if(TempNodo.Nombre==n){
                   this.Nodos.remove(TempNodo);
                   System.out.println("Nodo Eliminado Exitosamente ");
                   break;
               }else{
               ContNodo=ContNodo+1;
               }
           }
           if(this.Nodos.size()==ContNodo){
               System.out.println("Nodo no encontrado");
           }
        }
    }
    public void ListarNodos(){
    for (Nodo TempNodo : this.Nodos){
        System.out.println(TempNodo.Nombre);
    }
    }
    
    public  List<Nodo> getNodos(){
        return Nodos;
    }
    public Nodo BuscarNodo(String NombreNodo){
        
        
       if(this.Nodos.isEmpty()){
           return null;
       }else{
           for (Nodo TempNodo : this.Nodos){
               if(TempNodo.Nombre==NombreNodo){
                   /*indexof retorna el valor del objeto nodo*/
                   return this.Nodos.get(this.Nodos.indexOf(TempNodo));
                }
           }
          
        }
        return null;
        
    }
}

