public class Analisis {

    public static void main(String[] args) {
        Grafo graf = new Grafo();
   
        graf.agregarNodo("A");
        graf.agregarNodo("B");
        graf.agregarNodo("C");
        graf.agregarNodo("D");
        graf.ListarNodos();
        
       /* graf.eliminarNodo("guau guau");*/
        System.out.println("******************************************+****************************");
        System.out.println(graf.BuscarNodo("A").Nombre);
        graf.BuscarNodo("A").AgregarAdyacente(graf.BuscarNodo("B"),1);
        graf.BuscarNodo("A").AgregarAdyacente(graf.BuscarNodo("C"),2);
        graf.BuscarNodo("A").ListarAdyacentes();
        graf.BuscarNodo("A").BuscarArco(graf.BuscarNodo("B"),1).ActualizarPeso(3);
        graf.BuscarNodo("A").ListarAdyacentes();
        graf.BuscarNodo("A").eliminarAdyacente(graf.BuscarNodo("B"),3);
        graf.BuscarNodo("A").eliminarAdyacente(graf.BuscarNodo("B"),1);
        graf.BuscarNodo("A").ListarAdyacentes();
        graf.BuscarNodo("A").ModificarAdyacente(graf.BuscarNodo("C"), 2, graf.BuscarNodo("A"), 4);
        graf.BuscarNodo("A").ListarAdyacentes();
        graf.BuscarNodo("A").ModificarAdyacente(graf.BuscarNodo("C"), 2, graf.BuscarNodo("A"), 4);
    }    
}  
