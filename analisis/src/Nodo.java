import java.util.ArrayList;
import java.util.List;

public class Nodo {

   //Atributos del la clase nodo
    String Nombre;
    private List<Arco> Arcos;
    //Constructor del nodo para crear un objeto
    public Nodo(String Valor){
       Nombre=Valor;
       Arcos = new ArrayList<Arco>();
    }
    public void RenombrarNodo(String ValNew  ){
       this.Nombre = ValNew;
    }
    public void AgregarAdyacente(Nodo Destino, Integer peso){
       Arco Nuevo = new Arco(this, Destino, peso);
       Arcos.add(Nuevo);
    }
   public void ListarAdyacentes(){
       for (Arco TempArco : this.Arcos){
        System.out.println(TempArco.getOrigen().Nombre +" "
                         + TempArco.getDestino().Nombre + " " 
                         + TempArco.getPeso().toString() );
    }
   }
       public Arco BuscarArco( Nodo Destino, Integer Peso){
            if(this.Arcos.isEmpty()){
           return null;
       }else{
           for (Arco TempArco : this.Arcos){
               if(TempArco.getOrigen()==this && TempArco.getDestino()==Destino && TempArco.getPeso()==Peso){
                   return this.Arcos.get(this.Arcos.indexOf(TempArco));
               }
           }
        }
        return null;
       }
        public void eliminarAdyacente(Nodo Destino, Integer Peso){
            
            Arco TempArco= new Arco();
            if(this.Arcos.isEmpty()){
           
           System.out.println("Nodo sin Adyacente ");
       }else{
                TempArco=this.BuscarArco(Destino, Peso);
                if(TempArco==null){
                    System.out.println("Arco no encontrado");
                }else{
                this.Arcos.remove(TempArco);
                System.out.println("Arco Eliminado Exitosamente ");
                }
            }
    }
        public void ModificarAdyacente(Nodo DestinoAntiguo, Integer PesoAntiguo,Nodo DestinoNuevo, Integer PesoNuevo){
            Arco TempArco= new Arco();
            Integer IndexArco ;
            if(this.Arcos.isEmpty()){
                System.out.println("Nodo sin Adyacente ");
            }else{
                TempArco=this.BuscarArco(DestinoAntiguo, PesoAntiguo);
                if(TempArco==null){
                    System.out.println("Arco no encontrado");
                }else{
                    IndexArco = this.Arcos.indexOf(TempArco);
                    
                    TempArco.setDestino(DestinoNuevo);
                    TempArco.ActualizarPeso(PesoNuevo);
                    this.Arcos.set(IndexArco, TempArco);
                    System.out.println("Arco Modificado Exitosamente ");
                }
            }
        }
   }
