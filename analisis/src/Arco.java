public class Arco {
    
        private Nodo origen;
 	private Nodo destino;
 	private Integer peso;
 	public Arco(Nodo ori, Nodo des, Integer pes)
 	{
 	origen = ori;
 	destino = des;
 	peso = pes;	
 	}

    public Arco() {
        origen = null;
 	destino = null;
 	peso = null;
        
    }
        
    public Nodo getOrigen() {
        return origen;
    }
    public void setOrigen(Nodo origen) {
        this.origen = origen;
    }
    public Nodo getDestino() {
        return destino;
    }
    
    public void setDestino(Nodo destino) {
        this.destino = destino;
    }
    public Integer getPeso(){
    return peso;
    }
    public void ActualizarPeso(Integer NewPeso){
        this.peso=NewPeso;
    }
    
    
}

